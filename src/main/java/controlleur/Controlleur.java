package controlleur;

import facade.FacadeParis;
import javafx.stage.Stage;
import modele.Pari;
import modele.Utilisateur;
import vues.*;

public class Controlleur {
    private Home home;
    private FacadeParis facadeParis;
    private Menu menu;
    private Matchs matchs;
    private Utilisateur user;
    private Match match;
    private modele.Match matchSelectionner;
    private ConfirmPari confirmPari;
    private Pari dernierPari;
    private MesParis paris;
    private Pari pariSelectrionner;
    private MonPari monPari;

    public Controlleur(Stage primaryStage, FacadeParis facadeParis)
    {
        this.facadeParis = facadeParis;
        home = Home.creerInstance(this, primaryStage);
        home.show();
    }

    public void goToMenu()
    {
        user = home.getUser();
        menu = Menu.creerInstance(this, new Stage());
        menu.show();
    }

    public void goToMatchs()
    {
        matchs = Matchs.creerInstance(this, new Stage());
        matchs.show();
    }

    public void gotToMatch(long idMatch)
    {
        matchSelectionner = facadeParis.getMatch(idMatch);
        match = Match.creerInstance(this, new Stage());
        match.show();
    }

    public void goToConfirmPari(long idPari)
    {
        dernierPari = facadeParis.getPari(idPari);
        confirmPari = ConfirmPari.creerInstance(this, new Stage());
        confirmPari.show();
    }

    public void goToParis()
    {
        paris = MesParis.creerInstance(this, new Stage());
        paris.show();
    }

    public void goToPari(long idPari)
    {
        pariSelectrionner = facadeParis.getPari(idPari);
        monPari = MonPari.creerInstance(this, new Stage());
        monPari.show();
    }

    public FacadeParis getFacadeParis() {
        return facadeParis;
    }

    public Utilisateur getUser() {
        return user;
    }

    public modele.Match getMatchSelectionner() {
        return matchSelectionner;
    }

    public Pari getDernierPari() { return dernierPari; }

    public Pari getPariSelectrionner() { return pariSelectrionner; }

    public MesParis getParis() { return paris; }
}
