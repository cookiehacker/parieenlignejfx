package vues;

import controlleur.Controlleur;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public class Menu {

    @FXML
    public GridPane monPane;
    @FXML
    public Button matchs;
    @FXML
    public Button paris;
    @FXML
    public Button deconnexion;
    @FXML
    public Label pseudo;
    private Controlleur controlleur;
    private Stage stage;

    public static Menu creerInstance(Controlleur controlleur, Stage stage) {
        URL location = Menu.class.getResource("/vues/Menu.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(location);
        try {
            fxmlLoader.load();

        } catch (IOException e) {
            e.printStackTrace();
        }

        Menu vue = fxmlLoader.getController();
        vue.setStage(stage);
        vue.setControlleur(controlleur);


        return vue;
    }

    public void show() {
        stage.setTitle("Paris en ligne");
        stage.setScene(new Scene(monPane, monPane.getWidth(), monPane.getHeight()));
        stage.show();
        pseudo.setText("Bonjour : " + controlleur.getUser().getLogin());
    }

    public void setControlleur(Controlleur controlleur) {
        this.controlleur = controlleur;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void voirMatchs(ActionEvent actionEvent) {
        controlleur.goToMatchs();
    }

    public void voirParis(ActionEvent actionEvent) {
        controlleur.goToParis();
    }

    public void deconnexion(ActionEvent actionEvent) {
        controlleur.getFacadeParis().deconnexion(controlleur.getUser().getLogin());
        stage.close();
    }
}
