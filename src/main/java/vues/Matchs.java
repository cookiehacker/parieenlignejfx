package vues;

import controlleur.Controlleur;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import modele.Match;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;

public class Matchs {

    @FXML
    public TableView<Match> table;
    @FXML
    public VBox monPane;
    private Controlleur controlleur;
    private Stage stage;

    public static Matchs creerInstance(Controlleur controlleur, Stage stage) {
        URL location = Matchs.class.getResource("/vues/Matchs.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(location);
        try {
            fxmlLoader.load();

        } catch (IOException e) {
            e.printStackTrace();
        }

        Matchs vue = fxmlLoader.getController();
        vue.setStage(stage);
        vue.setControlleur(controlleur);

        return vue;
    }

    public void show() {
        stage.setTitle("Paris en ligne");
        stage.setScene(new Scene(monPane, monPane.getWidth(), monPane.getHeight()));
        stage.show();
        initialisationDesMatchs();
    }

    public void initialisationDesMatchs() {
        Collection<Match> mesMatch = controlleur.getFacadeParis().getAllMatchs();
        table.getItems().addAll(mesMatch);
        preparerTableView();
    }

    private void preparerTableView() {
        this.table.setEditable(false);


        this.table.setRowFactory(tv -> {
            TableRow<Match> row = new TableRow<Match>();
            row.setOnMouseClicked(mouseEvent -> {
                doForClick(mouseEvent, row);
            });
            return row;
        });


        TableColumn<Match, String> type = new TableColumn<>("Type");
        TableColumn<Match, String> joueur1 = new TableColumn<>("Joueur1");
        TableColumn<Match, String> joueur2 = new TableColumn<>("Joueur2");

        type.setCellValueFactory(
                new PropertyValueFactory<>("sport")
        );

        joueur1.setCellValueFactory(
                new PropertyValueFactory<>("equipe1")
        );

        joueur2.setCellValueFactory(
                new PropertyValueFactory<>("equipe2")
        );

        this.table.getColumns().addAll(type, joueur1, joueur2);
    }

    private void doForClick(MouseEvent e, TableRow<Match> table) {
        if (e.getClickCount() == 2 && e.getButton() == MouseButton.PRIMARY && !table.isEmpty()) {
            controlleur.gotToMatch(table.getItem().getIdMatch());
        }
    }

    public void setControlleur(Controlleur controlleur) {
        this.controlleur = controlleur;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
