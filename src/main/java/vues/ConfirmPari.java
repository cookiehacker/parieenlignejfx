package vues;

import controlleur.Controlleur;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public class ConfirmPari {

    @FXML
    public Label pseudo;
    @FXML
    public Label descriptif;
    @FXML
    public VBox monPane;
    private Controlleur controlleur;
    private Stage stage;

    public static ConfirmPari creerInstance(Controlleur controlleur, Stage stage) {
        URL location = ConfirmPari.class.getResource("/vues/confirmPari.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(location);
        try {
            fxmlLoader.load();

        } catch (IOException e) {
            e.printStackTrace();
        }

        ConfirmPari vue = fxmlLoader.getController();
        vue.setStage(stage);
        vue.setControlleur(controlleur);


        return vue;
    }

    public void show() {
        stage.setTitle("Paris en ligne");
        stage.setScene(new Scene(monPane, monPane.getWidth(), monPane.getHeight()));
        stage.show();
        initialiserPari();
    }

    public void initialiserPari() {
        pseudo.setText(controlleur.getUser().getLogin());
        descriptif.setText(
                "Vous avez parié " +
                        controlleur.getDernierPari().getMontant() +
                        " euros sur le résultat " +
                        controlleur.getDernierPari().getVainqueur() +
                        " pour le match : " +
                        controlleur.getDernierPari().getMatch().getEquipe1() +
                        " vs " +
                        controlleur.getDernierPari().getMatch().getEquipe2() +
                        " le " +
                        controlleur.getDernierPari().getMatch().getQuand().toString()
        );
    }

    public void setControlleur(Controlleur controlleur) {
        this.controlleur = controlleur;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
