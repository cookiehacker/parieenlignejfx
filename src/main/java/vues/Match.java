package vues;

import controlleur.Controlleur;
import facade.exceptions.MatchClosException;
import facade.exceptions.ResultatImpossibleException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public class Match {

    @FXML
    public Label sport;
    @FXML
    public Label eq1;
    @FXML
    public Label eq2;
    @FXML
    public Label date;
    @FXML
    public Label result;
    @FXML
    public VBox monPane;
    @FXML
    public TextField montant;
    @FXML
    public TextField verdict;
    private Controlleur controlleur;
    private Stage stage;

    public static Match creerInstance(Controlleur controlleur, Stage stage) {
        URL location = Match.class.getResource("/vues/Match.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(location);
        try {
            fxmlLoader.load();

        } catch (IOException e) {
            e.printStackTrace();
        }

        Match vue = fxmlLoader.getController();
        vue.setStage(stage);
        vue.setControlleur(controlleur);


        return vue;
    }

    public void show() {
        stage.setTitle("Paris en ligne");
        stage.setScene(new Scene(monPane, monPane.getWidth(), monPane.getHeight()));
        stage.show();
        initialisationMatch();
    }

    public void initialisationMatch() {
        modele.Match matchselectionne = this.controlleur.getMatchSelectionner();
        sport.setText(matchselectionne.getSport());
        eq1.setText(matchselectionne.getEquipe1());
        eq2.setText(matchselectionne.getEquipe2());
        date.setText(matchselectionne.getQuand().toString());
        result.setText(matchselectionne.getResultat().orElse("NONE"));
    }

    public void setControlleur(Controlleur controlleur) {
        this.controlleur = controlleur;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void parier(ActionEvent actionEvent) {
        try {
            controlleur.getParis().getTable().getItems().removeAll(controlleur.getFacadeParis().getMesParis(controlleur.getUser().getLogin()));
            long idpari = controlleur.getFacadeParis().parier(
                    controlleur.getUser().getLogin(),
                    controlleur.getMatchSelectionner().getIdMatch(),
                    verdict.getText(),
                    Integer.parseInt(montant.getText())
            );
            controlleur.getParis().getTable().getItems().addAll(controlleur.getFacadeParis().getMesParis(controlleur.getUser().getLogin()));
            controlleur.goToConfirmPari(idpari);

            stage.close();


        } catch (MatchClosException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Erreur de math", ButtonType.OK);
            alert.showAndWait();
        } catch (ResultatImpossibleException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Erreur de resultat", ButtonType.OK);
            alert.showAndWait();
        }
    }
}
