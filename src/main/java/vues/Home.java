package vues;

import controlleur.Controlleur;
import facade.exceptions.InformationsSaisiesIncoherentesException;
import facade.exceptions.UtilisateurDejaConnecteException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import modele.Utilisateur;

import java.io.IOException;
import java.net.URL;

public class Home {

    @FXML
    public Pane monPane;
    @FXML
    public TextField pseudo;
    @FXML
    public TextField motdepasse;
    private Controlleur controlleur;
    private Stage stage;
    private Utilisateur user;

    public static Home creerInstance(Controlleur controlleur, Stage stage) {
        URL location = Home.class.getResource("/vues/Home.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(location);
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Home vue = fxmlLoader.getController();
        vue.setStage(stage);
        vue.setControlleur(controlleur);

        return vue;
    }

    public void show() {
        stage.setTitle("Paris en ligne");
        stage.setScene(new Scene(monPane, monPane.getWidth(), monPane.getHeight()));
        stage.show();
    }

    public void setControlleur(Controlleur controlleur) {
        this.controlleur = controlleur;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void connection(ActionEvent actionEvent) {
        try {
            user = controlleur.getFacadeParis().connexion(pseudo.getText(), motdepasse.getText());
            controlleur.goToMenu();
        } catch (UtilisateurDejaConnecteException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Utilisateur déjà connecter !", ButtonType.OK);
            alert.showAndWait();
        } catch (InformationsSaisiesIncoherentesException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Information incoherentes", ButtonType.OK);
            alert.showAndWait();
        }
    }

    public Utilisateur getUser() {
        return user;
    }
}
