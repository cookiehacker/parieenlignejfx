package vues;

import controlleur.Controlleur;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import modele.Match;
import modele.Pari;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;

public class MesParis {

    @FXML
    public TableView<Pari> table;
    @FXML
    public VBox monPane;
    private Controlleur controlleur;
    private Stage stage;
    private ObservableList<Pari> observableList = FXCollections.observableArrayList();

    public static MesParis creerInstance(Controlleur controlleur, Stage stage) {
        URL location = MesParis.class.getResource("/vues/MesParis.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(location);
        try {
            fxmlLoader.load();

        } catch (IOException e) {
            e.printStackTrace();
        }

        MesParis vue = fxmlLoader.getController();
        vue.setStage(stage);
        vue.setControlleur(controlleur);

        return vue;
    }

    public void show() {
        stage.setTitle("Paris en ligne");
        stage.setScene(new Scene(monPane, monPane.getWidth(), monPane.getHeight()));
        stage.show();
        initialisationDesMatchs();
    }

    public void initialisationDesMatchs() {
        Collection<Pari> mesParis = controlleur.getFacadeParis().getMesParis(controlleur.getUser().getLogin());
        table.getItems().addAll(mesParis);
        preparerTableView();
    }

    private void preparerTableView() {
        this.table.setEditable(false);


        this.table.setRowFactory(tv -> {
            TableRow<Pari> row = new TableRow<>();
            row.setOnMouseClicked(mouseEvent -> {
                doForClick(mouseEvent, row);
            });
            return row;
        });

        TableColumn<Pari, Match> id = new TableColumn<>("idPari");
        TableColumn<Pari, String> vainqueur = new TableColumn<>("Vainqueur");
        TableColumn<Pari, String> montant = new TableColumn<>("Montant");

        id.setCellValueFactory(
                new PropertyValueFactory<>("idPari")
        );

        vainqueur.setCellValueFactory(
                new PropertyValueFactory<>("vainqueur")
        );

        montant.setCellValueFactory(
                new PropertyValueFactory<>("montant")
        );


        this.table.getColumns().addAll(id, vainqueur, montant);
    }

    private void doForClick(MouseEvent e, TableRow<Pari> table) {
        if (e.getClickCount() == 2 && e.getButton() == MouseButton.PRIMARY && !table.isEmpty()) {
            controlleur.goToPari(table.getItem().getIdPari());
        }
    }

    public void setControlleur(Controlleur controlleur) {
        this.controlleur = controlleur;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public TableView<Pari> getTable() {
        return table;
    }
}
