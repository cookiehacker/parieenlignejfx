package vues;

import controlleur.Controlleur;
import facade.exceptions.OperationNonAuthoriseeException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public class MonPari {

    @FXML
    public Label verdict;
    @FXML
    public Label mise;
    @FXML
    public Label date;
    @FXML
    public Label eq2;
    @FXML
    public Label eq1;
    @FXML
    public Label sport;
    @FXML
    public VBox monPane;
    private Controlleur controlleur;
    private Stage stage;

    public static MonPari creerInstance(Controlleur controlleur, Stage stage) {
        URL location = MonPari.class.getResource("/vues/MonPari.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(location);
        try {
            fxmlLoader.load();

        } catch (IOException e) {
            e.printStackTrace();
        }

        MonPari vue = fxmlLoader.getController();
        vue.setStage(stage);
        vue.setControlleur(controlleur);


        return vue;
    }

    public void show() {
        stage.setTitle("Paris en ligne");
        stage.setScene(new Scene(monPane, monPane.getWidth(), monPane.getHeight()));
        stage.show();
        initialisationPari();
    }

    public void initialisationPari() {
        sport.setText(controlleur.getPariSelectrionner().getMatch().getSport());
        eq1.setText(controlleur.getPariSelectrionner().getMatch().getEquipe1());
        eq2.setText(controlleur.getPariSelectrionner().getMatch().getEquipe2());
        date.setText(controlleur.getPariSelectrionner().getMatch().getQuand().toString());
        mise.setText(String.valueOf(controlleur.getPariSelectrionner().getMontant()));
        verdict.setText(controlleur.getPariSelectrionner().getVainqueur());
    }

    public void setControlleur(Controlleur controlleur) {
        this.controlleur = controlleur;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void annulerPari(ActionEvent actionEvent) {
        try {
            controlleur.getParis().getTable().getItems().removeAll(controlleur.getFacadeParis().getMesParis(controlleur.getUser().getLogin()));
            controlleur.getFacadeParis().annulerPari(controlleur.getUser().getLogin(), controlleur.getPariSelectrionner().getIdPari());
            controlleur.getParis().getTable().getItems().addAll(controlleur.getFacadeParis().getMesParis(controlleur.getUser().getLogin()));
            stage.close();
        } catch (OperationNonAuthoriseeException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Opération non authorisee", ButtonType.OK);
            alert.showAndWait();
        }
    }
}
