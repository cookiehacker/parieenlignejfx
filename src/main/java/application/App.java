package application;

import controlleur.Controlleur;
import facade.FacadeParis;
import facade.FacadeParisStaticImpl;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application {

    private static Scene scene;

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) throws Exception {
        FacadeParis facadeParis = new FacadeParisStaticImpl();
        Controlleur controlleur = new Controlleur(stage, facadeParis);
    }
}
